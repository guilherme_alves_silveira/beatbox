package com.main;

import com.beat.BeatBox;
import javax.swing.JOptionPane;

public class MainBeatBox
{
    private static boolean foiIniciadoIncorretamente;
    private static String userIdScreenName;
    private static String mensagem;
    
    public static void main(String... args)
    {
        iniciaBeatBox();
    }

    private static void iniciaBeatBox()
    {
        if(!foiIniciadoIncorretamente)
        {
            mensagem = "";
        }
        else
        {
            mensagem = "Por favor, preencha corretamente.\n";
        }
        
        userIdScreenName = JOptionPane.showInputDialog(null, mensagem + "Digite o seu código ou o nome de sua tela.");
        
        if (!isInvalido(userIdScreenName))
        {
            new BeatBox().startUp(userIdScreenName);
        }
        else
        {
            foiIniciadoIncorretamente = true;
            iniciaBeatBox();
        }
    }
    
    private static boolean isInvalido(String userIdScreenName)
    {
        return  userIdScreenName == null || userIdScreenName.trim().equals("");
    }
}