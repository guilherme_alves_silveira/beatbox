package com.beat;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Baseado no livro Use a Cabeça - Java
 * @author A-IKASORUK
 */
public class BeatBox
{
    private int nextNum;
    private String userName;
    //**********************************
    private final String[] instrumentsName =
    {
        "Bass Drum", "Closed Hi-Hat", "Open Hi-Hat", "Acoustic Cymbal",
        "Hand Clap", "High Tom", "Hi Bong", "Maracas", "Whistle", "Low Conga",
        "Cowbel", "Vibraslap", "Low-mid Tom", "High Agogo", "Open Hi Conga"
    };
    private final int[] instruments =
    {
        35, 42, 46, 38, 49, 39, 50, 60, 70, 72, 64, 56, 58, 47, 67, 63
    };
    //**********************************
    private final List<JCheckBox> chkList;
    private final Vector<String> lista;
    private final Map<String, boolean[]> otherSeqsMap;
    //**********************************
    private ObjectOutputStream out;
    private ObjectInputStream in;
    //**********************************
    private Sequencer sequencer;
    private Sequence sequence;
    private Sequence mySequence;
    private Track track;
    //**********************************
    private JFrame frmMain;
    private JPanel pnMain;
    private JList ltIncoming;
    private JScrollPane spScroll;
    private JTextField txtUserMessage;
    //**********************************
    private JButton btnStart;
    private JButton btnStop;
    private JButton btnUpTempo;
    private JButton btnDownTempo;
    private JButton btnSendIt;
    private JButton btnRestore;
    //**********************************
    private Box itensBox;
    private Box namesBox;
    //**********************************
    private JPanel background;

    public BeatBox()
    {
        this.chkList = new ArrayList<>();
        this.lista = new Vector<>();
        this.otherSeqsMap = new HashMap<>();
    }

    public void startUp(String name)
    {
        this.userName = name;
        try
        {
            Socket sock = new Socket("127.0.0.1", 4242);
            this.out = new ObjectOutputStream(sock.getOutputStream());
            this.in = new ObjectInputStream(sock.getInputStream());
            Thread remote = new Thread(new RemoteReader());
            remote.start();
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null,
                                          "Não foi possível conectar com o servidor, você terá de jogar sozinho.");
        }
        setUpMidi();
        buildGUI();
    }

    private void buildGUI()
    {
        this.frmMain = new JFrame("Cyber Beat Box");
        BorderLayout layout = new BorderLayout();
        this.background = new JPanel(layout);
        background.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        initItens();
        initActionListenerButtons();
        initIncomingList();
        initItensBox();
        initNamesBox();
        initMainPanel();
        posicioaLayouts();
    }

    private void initItens()
    {
        this.btnStart = new JButton("Inicia");
        this.btnStop = new JButton("Parar");
        this.btnUpTempo = new JButton("+ Tempo");
        this.btnDownTempo = new JButton("- Tempo");
        this.btnSendIt = new JButton("Enviar");
        this.btnRestore = new JButton("Restaurar");
        this.ltIncoming = new JList();
        this.txtUserMessage = new JTextField();
        this.spScroll = new JScrollPane(ltIncoming);
    }

    private void posicioaLayouts()
    {
        background.add(BorderLayout.EAST, itensBox);
        background.add(BorderLayout.WEST, namesBox);
        background.add(BorderLayout.CENTER, pnMain);
        //**********************************
        frmMain.getContentPane().add(background);
        frmMain.setBounds(50, 50, 300, 300);
        frmMain.pack();
        frmMain.setVisible(true);
    }

    private void initNamesBox()
    {
        this.namesBox = new Box(BoxLayout.Y_AXIS);

        for (String insName : instrumentsName)
        {
            namesBox.add(new Label(insName));
        }
    }

    private void initMainPanel()
    {
        GridLayout grid = new GridLayout(16, 16);
        grid.setVgap(1);
        grid.setHgap(2);
        this.pnMain = new JPanel(grid);

        for (int i = 0; i < 256; i++)
        {
            JCheckBox chkNovo = new JCheckBox();
            chkNovo.setSelected(false);
            chkList.add(chkNovo);
            pnMain.add(chkNovo);
        }
    }

    private void initItensBox()
    {
        this.itensBox = new Box(BoxLayout.Y_AXIS);
        itensBox.add(btnStart);
        itensBox.add(btnStop);
        itensBox.add(btnUpTempo);
        itensBox.add(btnDownTempo);
        itensBox.add(btnSendIt);
        itensBox.add(btnRestore);
        itensBox.add(txtUserMessage);
        itensBox.add(spScroll);
    }

    private void initIncomingList()
    {
        ltIncoming.addListSelectionListener(new MyListSelectionListener());
        ltIncoming.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ltIncoming.setListData(lista);
    }

    private void initActionListenerButtons()
    {
        btnStart.addActionListener(new MyStartListener());
        btnStop.addActionListener(new MyStopListener());
        btnUpTempo.addActionListener(new MyUpTempoListener());
        btnDownTempo.addActionListener(new MyDowmTempoListener());
        btnSendIt.addActionListener(new MySendListener());
        btnRestore.addActionListener(new MyPlayMineListener());
    }

    private void setUpMidi()
    {
        try
        {
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
            sequence = new Sequence(Sequence.PPQ, 4);
            track = sequence.createTrack();
            sequencer.setTempoInBPM(120);
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void buildTrackAndStart()
    {
        List<Integer> trackList = null;
        sequence.deleteTrack(track);
        track = sequence.createTrack();

        for (int i = 0; i < 16; i++)
        {
            trackList = new ArrayList<>();

            for (int j = 0; j < 16; j++)
            {
                //Vai de 0 a 255
                JCheckBox chkNovo = (JCheckBox) chkList.get(j + (16 * i));
                if (chkNovo.isSelected())
                {
                    int key = instruments[i];
                    trackList.add(key);
                }
                else
                {
                    trackList.add(null);
                }
            }
            makeTracks(trackList);
        }

        track.add(makeEvent(192, 9, 1, 0, 15));

        try
        {
            sequencer.setSequence(sequence);
            sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
            sequencer.start();
            sequencer.setTempoInBPM(120);
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private class RemoteReader implements Runnable
    {

        private boolean[] chkState;
        private String nameToShow;
        Object obj;

        @Override
        public void run()
        {
            try
            {
                while ((obj = in.readObject()) != null)
                {
                    System.out.println("Chegou um objeto do servidor!");
                    System.out.println(obj.getClass());
                    this.nameToShow = (String) obj;
                    chkState = (boolean[]) in.readObject();
                    otherSeqsMap.put(nameToShow, chkState);
                    lista.add(nameToShow);
                    ltIncoming.setListData(lista);
                }
            }
            catch (Exception ex)
            {
                JOptionPane.showMessageDialog(null, "Ocorrência:" + ex.getMessage());
            }
        }
    }

    private class MySendListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            boolean[] chkState = new boolean[256];
            for (int i = 0; i < 256; i++)
            {
                JCheckBox chkNovo = (JCheckBox) chkList.get(i);
                if (chkNovo.isSelected())
                {
                    chkState[i] = true;
                }
            }

            String msgAEnviar = txtUserMessage.getText();

            try
            {
                out.writeObject(userName + nextNum++ + ": " + msgAEnviar);
                out.writeObject(chkState);
            }
            catch (Exception ex)
            {
                JOptionPane.showMessageDialog(null, "Não foi possível enviar o arquivo!");
            }

            txtUserMessage.setText("");
        }
    }

    private class MyDowmTempoListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            alteraTempoFactory(0.97f);
        }
    }

    private class MyUpTempoListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            alteraTempoFactory(1.03f);
        }
    }

    private class MyStopListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            sequencer.stop();
        }
    }

    private class MyStartListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            buildTrackAndStart();
        }
    }

    private class MyListSelectionListener implements ListSelectionListener
    {

        @Override
        public void valueChanged(ListSelectionEvent le)
        {
            if (!le.getValueIsAdjusting())
            {
                String selected = (String) ltIncoming.getSelectedValue();
                if (selected != null)
                {
                    boolean[] selectedState = (boolean[]) otherSeqsMap.get(selected);
                    changeSequence(selectedState);
                    sequencer.stop();
                    buildTrackAndStart();
                }
            }
        }
    }

    private class MyPlayMineListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (mySequence != null)
            {
                sequence = mySequence;
            }
        }
    }

    private void alteraTempoFactory(float multiplicadoPor)
    {
        float tempoFactor = sequencer.getTempoFactor();
        sequencer.setTempoFactor((float) (tempoFactor * multiplicadoPor));
    }

    private void changeSequence(boolean[] selectedState)
    {
        for (int i = 0; i < 256; i++)
        {
            JCheckBox chkNovo = (JCheckBox) chkList.get(i);
            if (selectedState[i])
            {
                chkNovo.setSelected(true);
            }
            else
            {
                chkNovo.setSelected(false);
            }
        }
    }

    private void makeTracks(List<Integer> trackList)
    {
        Iterator it = trackList.iterator();
        for (int i = 0; i < 16; i++)
        {
            Integer numKey = (Integer) it.next();
            if (numKey != null)
            {
                track.add(makeEvent(144, 9, numKey, 100, i));
                track.add(makeEvent(128, 9, numKey, 100, i + 1));
            }
        }
    }

    private MidiEvent makeEvent(int comd, int chan, int one, int two, int tick)
    {
        MidiEvent event = null;
        try
        {
            ShortMessage msg = new ShortMessage();
            msg.setMessage(comd, chan, one, two);
            event = new MidiEvent(msg, tick);
        }
        catch (Exception ex)
        {
            //Ignora
        }
        return event;
    }
}