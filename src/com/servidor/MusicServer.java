package com.servidor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MusicServer
{
    private List<ObjectOutputStream> clientOut;

    public void inicia()
    {
        System.out.print("Iniciando...");
        this.clientOut = new ArrayList<>();
        
        try
        {
            ServerSocket serverSocket = new ServerSocket(4242);
            while(true)
            {
                Socket clientSocket = serverSocket.accept();
                ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());
                clientOut.add(out);
                Thread thread = new Thread(new ClienteHandler(clientSocket));
                thread.start();
                
                System.out.println("Iniciou a conexão!");
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void tellEveryone(Object one, Object two)
    {
        Iterator it = clientOut.iterator();
        while(it.hasNext())
        {
            try
            {
                ObjectOutputStream out = (ObjectOutputStream) it.next();
                out.writeObject(one);
                out.writeObject(two);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
    
    private class ClienteHandler implements Runnable
    {

        private ObjectInputStream in;
        private Socket clientSocket;

        public ClienteHandler(Socket socket)
        {
            try
            {
                this.clientSocket = socket;
                this.in = new ObjectInputStream(clientSocket.getInputStream());
            }
            catch(IOException ex) 
            {
                ex.printStackTrace();
            }
        }

        @Override
        public void run()
        {
            Object o1 = null;
            Object o2 = null;
            
            try
            {
                while((o1 = in.readObject()) != null)
                {
                    o2 = in.readObject();
                    System.out.println("Leu dois objetos");
                    tellEveryone(o1, o2);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
